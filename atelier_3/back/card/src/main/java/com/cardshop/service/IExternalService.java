package com.cardshop.service;

import com.lib.model.TokenPublic;
import org.springframework.http.ResponseEntity;


public interface IExternalService {
    ResponseEntity requestFindTokenWithTokenHeader(String tokenPublic);
    ResponseEntity requestUserById(Long userId, String tokenHeader);
    ResponseEntity requestUpdateUserMoney(Long userId, Integer newBalance, String tokenHeader);
}
