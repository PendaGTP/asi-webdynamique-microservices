package com.cardshop.service;

import com.cardshop.model.Card;
import com.lib.form.StoreCardForm;

import java.util.List;
import java.util.Optional;

public interface ICardService {
    Card addCard(StoreCardForm storeCardForm, Long userId);

    List<Card> getCards(Long userId);

    Optional<Card> getCard(Long cardId);

    void updateCard(Card card);
}
