package com.cardshop.responses;

import com.cardshop.model.Token;

import java.time.LocalDateTime;

public class TokenResponse extends AbstractResponse {
    private long userId;
    private Token token;

    public TokenResponse(long userId, Token token) {
        this.userId = userId;
        this.token = token;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
}
