// attendre le chargement de la page
$(document ).ready(function() {

    // EVENT : click sur le bouton submit
    $("#submit").on("click", () => {
        requestSignin($("#username").val(), $("#email").val(), $("#password").val(),  $("#passwordRetype").val());
    });


});

function requestSignin(name, email, password, password2){

    if(password !== password2){
        alert("not same password");
    }else{

        $.ajax({
            url: "http://localhost:8888/auth/register",
            type: "POST",
            data: JSON.stringify({
                name,
                password,
                email
            }),
            contentType: "application/json",
            dataType: "json",
            success: function (data, textStatus, request) {
                console.log(data);
                let headers = request.getResponseHeader('Authorization');
                console.log(headers);
                Cookies.set("token", data['token']['token']);
                document.location = "../cardHome.html"; //http://localhost:8080
            },
            error: function(e) {
                console.log(e)
            }
        });
    }
}
