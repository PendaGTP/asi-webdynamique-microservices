package com.cardshop.controller;

import com.cardshop.form.LoginForm;
import com.cardshop.form.RegisterForm;
import com.cardshop.model.Token;
import com.cardshop.model.User;
import com.cardshop.repository.TokenRepository;
import com.cardshop.repository.UserRepository;
import com.cardshop.responses.ErrorResponse;
import com.cardshop.responses.HttpResponse;
import com.cardshop.responses.TokenResponse;
import com.cardshop.service.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.cardshop.service.AuthService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private AuthService authService;
    @Autowired
    private TokenManager tokenManager;

    @PostMapping(value = "/login", produces = "application/json")
    @ResponseBody
    ResponseEntity login(@RequestBody LoginForm loginForm) {
        User user = authService.login(loginForm.getName(), loginForm.getPassword());
        if(user != null) {
            Token token = tokenManager.updateOrCreateTokenForUser(user);
            TokenResponse response = new TokenResponse(user.getId(), token);
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.status(401).body(new HttpResponse(
                401, "La combinaison utilisateur / mot de passe ne correspond pas"
        ));
    }

    @PostMapping(value = "/register", produces = "application/json")
    @ResponseBody
    ResponseEntity register(@RequestBody RegisterForm registerForm) {
        List<User> usersMatches = userRepository.findUsersByName(registerForm.getName());
        List<User> emailMatches = userRepository.findUsersByEmail(registerForm.getEmail());
        List<String> errors = new ArrayList<>();;
        if(usersMatches.size() != 0) {
            errors.add("user name already exists");
        }

        if(registerForm.getPassword().length() <= 8) {
            errors.add("password yould ahve more than 8 characters");
        }

        // TODO email verification with regex

        if(emailMatches.size() != 0) {
            errors.add("An account with this email already exists");
        }

        if(errors.size() == 0) {
            User user = new User();
            user.setName(registerForm.getName());
            user.setMail(registerForm.getEmail());
            user.setPassword(registerForm.getPassword()); // TODO: hash password
            user.setBalance(5000);
            userRepository.save(user);
            Token token = tokenManager.createTokenForUser(user);
            return ResponseEntity.ok(new TokenResponse(
                    user.getId(), token
            ));
        }

        return ResponseEntity.status(400).body(new ErrorResponse(errors));
    }

    @GetMapping(value = "/me", produces = "application/json")
    @ResponseBody
    ResponseEntity me(@RequestHeader("Authorization") String tokenHeader) {
        Token token = tokenRepository.findTokenByToken(tokenHeader);
        if (token != null) {
            return ResponseEntity.ok(token.getUser());
        }
        return ResponseEntity.status(401).body(new HttpResponse(
                401, "Access non autorisé"
        ));
    }

}
