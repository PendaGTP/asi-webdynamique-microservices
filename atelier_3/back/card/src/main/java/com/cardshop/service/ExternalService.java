package com.cardshop.service;

import com.lib.model.TokenPublic;
import com.lib.model.UserPublic;
import com.lib.responses.ErrorResponse;
import com.lib.responses.HttpResponse;
import com.lib.responses.TokenResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExternalService implements IExternalService{

    @Override
    public ResponseEntity requestFindTokenWithTokenHeader(String tokenHeader) {
        System.out.println("ExternalService : requestFindTokenWithTokenHeader for tokenHeader :"+tokenHeader);

        // Array pour les erreurs
        List<String> errors = new ArrayList<>();

        // -- Token-Service config
        // header setup
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", tokenHeader);
        HttpEntity headersHttp = new HttpEntity(headers);

        RestTemplate restTemplate = new RestTemplate();
        String tokenResourceUrl = "http://token:8484/token";

        // Envoie de la requête
        ResponseEntity<TokenPublic> response = restTemplate.exchange(
                tokenResourceUrl, HttpMethod.GET, headersHttp, TokenPublic.class);
        System.out.println("TEST --> "+response.getStatusCodeValue());
        // Gestion de la réponse
        if (response.getStatusCodeValue() == 200){
            return response;
        }else if(response.getStatusCodeValue() == 401){
            return ResponseEntity.status(401).body(new HttpResponse(401, "Access non autorisé"));
        }else {
            errors.add("an error occur in server side");
            return ResponseEntity.status(response.getStatusCodeValue()).body(new ErrorResponse(errors));
        }
    }

    @Override
    public ResponseEntity requestUserById(Long userId, String tokenHeader) {
        System.out.println("ExternalService : requestFindTokenWithTokenHeader for tokenHeader :"+tokenHeader);

        // Array pour les erreurs
        List<String> errors = new ArrayList<>();

        // -- Token-Service config
        // header setup
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", tokenHeader);
        HttpEntity headersHttp = new HttpEntity(headers);

        RestTemplate restTemplate = new RestTemplate();
        String tokenResourceUrl = "http://user:8383/users/"+userId;

        // Envoie de la requête
        ResponseEntity<UserPublic> response = restTemplate.exchange(
                tokenResourceUrl, HttpMethod.GET, headersHttp, UserPublic.class);
        // Gestion de la réponse
        if (response.getStatusCodeValue() == 200){
            return response;
        }else if(response.getStatusCodeValue() == 401){
            return ResponseEntity.status(401).body(new HttpResponse(401, "Access non autorisé"));
        }else {
            errors.add("an error occur in server side");
            return ResponseEntity.status(response.getStatusCodeValue()).body(new ErrorResponse(errors));
        }
    }


    public ResponseEntity requestUpdateUserMoney(Long userId, Integer newBalance, String tokenHeader){
        System.out.println("ExternalService : requestUpdateUserMoney(Long userId, Integer newBalance)");

        // Array pour les erreurs
        List<String> errors = new ArrayList<>();

        // -- Token-Service config
        // header setup
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", tokenHeader);
        HttpEntity headersHttp = new HttpEntity(headers);

        RestTemplate restTemplate = new RestTemplate();
        String tokenResourceUrl = "http://user:8383/users/"+userId+"?newBalance="+newBalance;

        // Envoie de la requête
        ResponseEntity<UserPublic> response = restTemplate.exchange(
                tokenResourceUrl, HttpMethod.PATCH, headersHttp, UserPublic.class);
        // Gestion de la réponse
        if (response.getStatusCodeValue() == 200){
            return response;
        }else if(response.getStatusCodeValue() == 401){
            return ResponseEntity.status(401).body(new HttpResponse(401, "Access non autorisé"));
        }else {
            errors.add("an error occur in server side");
            return ResponseEntity.status(response.getStatusCodeValue()).body(new ErrorResponse(errors));
        }
    }
}
