package com.cardshop.model;

// imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price

import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Table(name = "games")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne()
    private Card card1;
    @ManyToOne()
    @Nullable
    private Card card2;

    @ManyToOne()
    private User player1;
    @ManyToOne()
    @Nullable
    private User player2;
    @ManyToOne()
    @Nullable
    private User winner;

    private Integer bet;

    public Integer getBet() {
        return bet;
    }

    public void setBet(Integer bet) {
        this.bet = bet;
    }
}
