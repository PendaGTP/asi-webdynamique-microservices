package com.cardshop.service;

import com.cardshop.model.Card;
import com.cardshop.model.Item;
import com.cardshop.repository.ItemRepository;
import com.lib.enums.ItemStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService implements IItemService{

    @Autowired
    ItemRepository itemRepository;

    @Override
    public List<Item> findItemsByStatus(ItemStatus itemStatus) {
        return itemRepository.findAllByStatus(itemStatus);
    }

    @Override
    public Item findItemByCard(Card card) {
        return itemRepository.findItemByCard(card);
    }

    @Override
    public Item addItem(Card card, Long userId) {
        Item item = new Item();
        item.setCard(card);
        item.setUserIdseller(userId);
        item.setStatus(ItemStatus.PENDING);
        return itemRepository.save(item);
    }

    @Override
    public List<Item> getItems() {
        return (List<Item>) itemRepository.findAll();
    }

    @Override
    public Optional<Item> getItem(Long id) {
        return itemRepository.findById(id);
    }

    @Override
    public void updateItem(Item item) {
        itemRepository.save(item);
    }


}
