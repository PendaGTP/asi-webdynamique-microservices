package com.cardshop.service;

import com.cardshop.model.User;
import com.cardshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService {
    @Autowired
    private UserRepository userRepository;

    public User login(String name, String password) {
        // TODO : hash password :)
        Optional<User> user = userRepository.findUserByNameAndPassword(name, password);
        if (user.isPresent()) {
            return user.get();
        }else {
            return null;
        }
    }


}
