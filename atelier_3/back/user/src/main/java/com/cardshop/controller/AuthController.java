package com.cardshop.controller;
import com.cardshop.model.User;
import com.cardshop.service.IExternalService;
import com.cardshop.service.IUserService;
import com.lib.form.LoginForm;
import com.lib.form.RegisterForm;
import com.lib.model.TokenPublic;
import com.lib.model.UserPublic;
import com.lib.responses.DataResponse;
import com.lib.responses.ErrorResponse;
import com.lib.responses.HttpResponse;
import com.lib.responses.TokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
public class AuthController {

    @Autowired
    private IUserService iUserService;

    @Autowired
    private IExternalService iExternalService;

    @RequestMapping(method = RequestMethod.POST, value = "/auth/login", produces = "application/json")
    ResponseEntity login(@RequestBody LoginForm loginForm) {
        System.out.println("AuthController - POST : /auth/login");
        List<String> errors = new ArrayList<>();

        // -- Find user related to username/password login
        User user = iUserService.login(loginForm.getName(), loginForm.getPassword());

        // -- create or update token if user exist
        if (user != null) {
            // -- Send request (token-service) to get or update token user
            return iExternalService.requestCreateOrUpdateToken(user);

        } else { // - if username/login not found
            return ResponseEntity.status(401).body(new HttpResponse(
                    401, "La combinaison utilisation / mot de passe ne correspond pas"
            ));
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/auth/register", produces = "application/json")
    ResponseEntity register(@RequestBody RegisterForm registerForm) throws IOException {
        System.out.println("AuthController - POST : /auth/register");
        List<String> errors = new ArrayList<>();

        // -- password verification
        if(!(iUserService.passwordComplexityValidation(registerForm.getPassword()))){
            errors.add("password is too short - 9 char required");
        }

        // -- username verification
        if(iUserService.userAlreadyExist(registerForm.getName())){
            errors.add("username is already taken");
        }

        // -- create new user with token if no error
        if(errors.size() == 0){
            // -- create user
            User user = iUserService.addUser(registerForm);

            // -- Send request (token-service) to create token
            return iExternalService.requestCreateToken(user);

        } else { return ResponseEntity.status(400).body(new ErrorResponse(errors)); }
    }


    @RequestMapping(method = RequestMethod.GET, value = "/auth/me", produces = "application/json")
    ResponseEntity me(@RequestHeader("Authorization") String tokenHeader) throws IOException {
        System.out.println("AuthController - GET : /auth/me");

        // - Send request (token-service) to check token
        ResponseEntity responseEntity = iExternalService.requestFindTokenWithTokenHeader(tokenHeader);

        // -- Find user by id
        if(responseEntity.getStatusCodeValue() == 200){
            // Convert ReponseEntity to ReponseEntity<UserPublic>
            ResponseEntity<TokenResponse> tokenPublicResponseEntity = responseEntity;
            System.out.println("CardController -  GET : /auth/me --> "+tokenPublicResponseEntity.getBody().getToken().getUserId());
            Long userId = tokenPublicResponseEntity.getBody().getToken().getUserId();

            // -- Find user by id
            Optional<User> user = iUserService.getuser(userId);
            if(user.isPresent()){
                return ResponseEntity.ok(user.get());
            } else {
                return ResponseEntity.status(420).body(new HttpResponse(
                        420, "user with id doesnt existe : "+userId
                ));
            }

        } else {
            return responseEntity;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/{id}", produces = "application/json")
    ResponseEntity getUser(@RequestHeader("Authorization") String tokenHeader, @PathVariable Long id){
        System.out.println("AuthController - GET : /users/{id} : "+id);

        // -- Send request (token-service) to get token
        ResponseEntity responseEntity = iExternalService.requestFindTokenWithTokenHeader(tokenHeader);

        // -- Find user by id
        if(responseEntity.getStatusCodeValue() == 200){
            // Convert ReponseEntity to ReponseEntity<UserPublic>
            ResponseEntity<UserPublic> userPublicResponseEntity = responseEntity;

            // -- Find user by id
            Optional<User> user = iUserService.getuser(id);
            if(user.isPresent()){
                return ResponseEntity.ok(user.get());
            } else {
                return ResponseEntity.status(420).body(new HttpResponse(
                        420, "user with id doesnt existe : "+id
                ));
            }

        } else {
            return responseEntity;
        }
    }

    @RequestMapping(method = RequestMethod.PATCH, value = "/users/{id}", produces = "application/json", params = "newBalance")
    ResponseEntity updateUserMoney(@RequestHeader("Authorization") String tokenHeader,@PathVariable Long id, @RequestParam Integer newBalance){
        System.out.println("AuthController - PATH : /users/{id} : "+id);

        // -- Send request (token-service) to get token
        ResponseEntity responseEntity = iExternalService.requestFindTokenWithTokenHeader(tokenHeader);

        // -- Find user by id
        if(responseEntity.getStatusCodeValue() == 200){
            // Convert ReponseEntity to ReponseEntity<UserPublic>
            ResponseEntity<UserPublic> userPublicResponseEntity = responseEntity;

            // -- Find user by id
            Optional<User> user = iUserService.getuser(id);
            if(user.isPresent()){
                System.out.println("Maj des sous de l'utilisateur");
                User user1 = user.get();
                user1.setBalance(newBalance);
                return ResponseEntity.ok(iUserService.updateUser(user1));
            } else {
                return ResponseEntity.status(420).body(new HttpResponse(
                        420, "user with id doesnt existe : "+id
                ));
            }

        } else {
            return responseEntity;
        }

    }
}
