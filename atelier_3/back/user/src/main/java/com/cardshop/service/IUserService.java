package com.cardshop.service;
import com.cardshop.model.User;
import com.lib.form.RegisterForm;
import java.util.List;
import java.util.Optional;


public interface IUserService {
   User addUser(RegisterForm registerForm);
   User updateUser(User user);
   List<User> getUsersByName(String name);
   Optional<User> getuser(Long userId);

   Boolean userAlreadyExist(String username);
   Boolean passwordComplexityValidation(String password);
   User login(String name, String password);
}
