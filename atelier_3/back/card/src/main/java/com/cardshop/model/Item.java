package com.cardshop.model;

import com.lib.enums.ItemStatus;
import org.springframework.lang.Nullable;
import com.cardshop.model.Card;
import javax.persistence.*;

@Entity
@Table(name="items")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne()
    private Card card;

    private Long userIdseller;

    @Nullable
    private Long userIdBuyer;

    @Enumerated(EnumType.ORDINAL)
    private ItemStatus status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserIdseller() {
        return userIdseller;
    }

    public void setUserIdseller(Long userIdseller) {
        this.userIdseller = userIdseller;
    }

    @Nullable
    public Long getUserIdBuyer() {
        return userIdBuyer;
    }

    public void setUserIdBuyer(@Nullable Long userIdBuyer) {
        this.userIdBuyer = userIdBuyer;
    }

    public ItemStatus getStatus() {
        return status;
    }

    public void setStatus(ItemStatus status) {
        this.status = status;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
