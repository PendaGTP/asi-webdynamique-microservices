package com.cardshop.enums;

public enum ItemStatus {
    PENDING, SOLD
}

