package com.cardshop.controller;



import com.cardshop.model.Card;
import com.cardshop.model.Item;
import com.cardshop.repository.CardRepository;
import com.cardshop.service.ICardService;
import com.cardshop.service.IExternalService;
import com.cardshop.service.IItemService;
import com.lib.enums.ItemStatus;
import com.lib.form.SellCardForm;
import com.lib.form.StoreCardForm;
import com.lib.model.TokenPublic;
import com.lib.model.UserPublic;
import com.lib.responses.DataResponse;
import com.lib.responses.ErrorResponse;
import com.lib.responses.HttpResponse;
import com.lib.responses.TokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class CardController {
    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private IExternalService iExternalService;

    @Autowired
    private ICardService iCardService;

    @Autowired
    private IItemService iItemService;


    // --------------------------------------- CARD SERVICE ---------------------------------------

    // -- Get cards for user based on token header
    @RequestMapping(method = RequestMethod.GET, value = "/api/v1/cards")
    ResponseEntity index(@RequestHeader("Authorization") String tokenHeader) {
        System.out.println("CardController - GET : /api/v1/cards");
        List<String> errors = new ArrayList<>();

        // -- Send request (token-service) to get token
        ResponseEntity responseEntity = iExternalService.requestFindTokenWithTokenHeader(tokenHeader);

        // -- Find cards with user-id if token if valid
        if(responseEntity.getStatusCodeValue() == 200){
            // Convert ResponseEntity to ResponseEntity<TokenPublic>
            ResponseEntity<TokenPublic> tokenPublicResponseEntity = responseEntity;

            // -- Find cards with user-id
            List<Card> cards = cardRepository.findCardsByUserId(tokenPublicResponseEntity.getBody().getUserId());
            return ResponseEntity.ok(new DataResponse(cards));
        }else {
            return responseEntity;
        }
    }

    // -- Create new cards
    @RequestMapping(method = RequestMethod.POST, value = "/api/v1/cards")
    ResponseEntity addCard(@RequestHeader("Authorization") String tokenHeader, @RequestBody StoreCardForm storeCardForm) {
        System.out.println("CardController - POST : /cards");
        // -- Send request (token-service) to get token
        ResponseEntity responseEntity = iExternalService.requestFindTokenWithTokenHeader(tokenHeader);

        // -- Find cards with user-id if token if valid
        if(responseEntity.getStatusCodeValue() == 200) {
            System.out.println("CardController - POST : /cards --> user find !");

            // Convert ResponseEntity to ResponseEntity<TokenPublic>
            ResponseEntity<TokenPublic> tokenPublicResponseEntity = responseEntity;

            // -- Add a new card
            Card card = iCardService.addCard(storeCardForm, tokenPublicResponseEntity.getBody().getUserId());

            // -- Get all card of this user
            List<Card> cards = iCardService.getCards(tokenPublicResponseEntity.getBody().getUserId());

            // - Send all card to client
            return ResponseEntity.ok(new DataResponse(cards));
        } else {
            return responseEntity;
        }
    }

    // -- Get cards by name
    @RequestMapping(method = RequestMethod.GET, value = "/api/v1/cards", params = "name")
    ResponseEntity getCardByName(@RequestHeader("Authorization") String tokenHeader, @RequestParam("name") String name) {
        System.out.println("CardController - GET : /api/v1/cards?name="+name);
        // -- Send request (token-service) to get token
        ResponseEntity responseEntity = iExternalService.requestFindTokenWithTokenHeader(tokenHeader);

        // -- Find cards with user-id if token if valid
        if(responseEntity.getStatusCodeValue() == 200) {
            System.out.println("CardController - GET : /api/v1/cards?name= -- find user");

            // Convert ResponseEntity to ResponseEntity<TokenPublic>
            ResponseEntity<TokenPublic> tokenPublicResponseEntity = responseEntity;

            List<Card> cards = cardRepository.findCardsByUserIdAndNameContainingIgnoreCase(tokenPublicResponseEntity.getBody().getUserId(), name);
            return ResponseEntity.ok(new DataResponse(cards));

        }else {
            return responseEntity;
        }
    }

    // --------------------------------------- ITEMS SERVICE ---------------------------------------

    // -- Get all items
    @RequestMapping(method = RequestMethod.GET, value = "/api/v1/items")
    ResponseEntity getItems(@RequestHeader("Authorization") String tokenHeader){
        System.out.println("CardController - GET : /api/v1/items");
        // -- Send request (token-service) to get token
        ResponseEntity responseEntity = iExternalService.requestFindTokenWithTokenHeader(tokenHeader);

        // -- Find items with user-id if token if valid
        if(responseEntity.getStatusCodeValue() == 200) {
            List<Item> items = iItemService.findItemsByStatus(ItemStatus.PENDING);
            return ResponseEntity.ok(new DataResponse(items));
        } else {
            return responseEntity;
        }
    }

    // -- Add a item to sold
    @RequestMapping(method = RequestMethod.POST, value = "/api/v1/items")
    ResponseEntity sellItem(@RequestHeader("Authorization") String tokenHeader, @RequestBody SellCardForm sellCardForm){
        System.out.println("CardController - POST : /api/v1/items");
        // -- Send request (token-service) to get token
        ResponseEntity responseEntity = iExternalService.requestFindTokenWithTokenHeader(tokenHeader);

        // -- Find items with user-id if token if valid
        if(responseEntity.getStatusCodeValue() == 200) {

            // get cards of user
            Optional<Card> card = iCardService.getCard(sellCardForm.getCard());

            if(card.isPresent()){

                // check if card is already in vente
                if(iItemService.findItemByCard(card.get()) != null){
                    return ResponseEntity.status(400).body(new HttpResponse(
                            400, "La carte est deja en vente !"
                    ));
                }

                // Convert ResponseEntity to ResponseEntity<TokenPublic>
                ResponseEntity<TokenPublic> tokenPublicResponseEntity = responseEntity;

                // create new item
                Item item = iItemService.addItem(card.get(), tokenPublicResponseEntity.getBody().getUserId());

                return ResponseEntity.ok(item.getCard());

            } else {
                return ResponseEntity.status(420).body(new HttpResponse(
                        409, "Cette item n'existe pas ou n\'est plus en vente"
                ));
            }

        } else {
            return responseEntity;
        }
    }


    // -- Buy an item
    @RequestMapping(method = RequestMethod.PATCH, value = "/api/v1/items/{id}")
    ResponseEntity buyItem(@RequestHeader("Authorization") String tokenHeader, @PathVariable Long id) {
        System.out.println("CardController - POST : /api/v1/items/{id}");

        // -- Send request (token-service) to get token
        ResponseEntity responseEntity = iExternalService.requestFindTokenWithTokenHeader(tokenHeader);

        // -- Find items with user-id if token if valid
        if(responseEntity.getStatusCodeValue() == 200) {


            System.out.println("CardController - POST : /api/v1/items/{id} --> Token is valid");
            Optional<Item> optionalItem = iItemService.getItem(id);
            if (optionalItem.isPresent() && optionalItem.get().getStatus() == ItemStatus.PENDING){
                System.out.println("CardController - POST : /api/v1/items/{id} --> l'item est bien en vente");
                Item item = optionalItem.get();
                Card card = item.getCard();

                // Convert ResponseEntity to ResponseEntity<TokenPublic>
                ResponseEntity<TokenPublic> tokenPublicResponseEntity = responseEntity;
                System.out.println("CardController - POST : /api/v1/items/{id} --> "+tokenPublicResponseEntity.getBody());

                // -- Send request (user-service) to get user by id
                ResponseEntity responseEntityRawUser = iExternalService.requestUserById(tokenPublicResponseEntity.getBody().getUserId(), tokenHeader);

                if(responseEntityRawUser.getStatusCodeValue() == 200){
                    // Convert ResponseEntity to ResponseEntity<TokenPublic>
                    ResponseEntity<UserPublic> responseEntityUser = responseEntityRawUser;
                    // Get UserPublic model
                    UserPublic userPublic = responseEntityUser.getBody();

                    // Check if user have enought money
                    if (userPublic.getBalance() >= card.getPrice()){
                        Integer newBalance = userPublic.getBalance() - card.getPrice();

                        // -- Send request (user-service) to update user money
                        ResponseEntity<UserPublic> userPublicResponseEntity = iExternalService.requestUpdateUserMoney(userPublic.getId(),newBalance,tokenPublicResponseEntity.getBody().getToken());
                        System.out.println("--- sur la money de l'utilisateur : "+userPublicResponseEntity.getBody().getId());

                        // Update card -> set new owner
                        card.setUserId(userPublic.getId());
                        iCardService.updateCard(card);

                        // Update item -> set new status and new owner
                        item.setUserIdBuyer(userPublic.getId());
                        item.setStatus(ItemStatus.SOLD);
                        iItemService.updateItem(item);

                        return ResponseEntity.ok(item.getCard());
                    }
                }
            }
            return ResponseEntity.status(420).body(new HttpResponse(
                    409, "Cette item n'existe pas ou n\'est plus en vente"
            ));
        } else {
            return responseEntity;
        }
    }
}
