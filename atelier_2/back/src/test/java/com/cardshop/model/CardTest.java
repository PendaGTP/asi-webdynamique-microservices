package com.cardshop.model;


import static org.junit.Assert.assertTrue;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CardTest {

    private List<String> stringList; // string attribut
    private List<Integer> integerList; // int attribut

    @Before
    public void setUp() {
        System.out.println("[BEFORE TEST] : -- Add Card to test");

        stringList = new ArrayList<String>();
        integerList = new ArrayList<Integer>();

        stringList.add("name");
        stringList.add("description");
        stringList.add("family");
        stringList.add("imgUrl");
        integerList.add(100);
        integerList.add(200);
        integerList.add(300);
        integerList.add(400);
        integerList.add(500);
    }

    @After
    public void cleanUp(){
        System.out.print("[AFTER TEST] - Clean hero list");
        stringList = null;
        integerList = null;
    }

    @Test
    public void createCard(){
        for(String msg:stringList) {
            for(String msg1:stringList) {
                for(String msg2:stringList) {
                    for(String msg3:stringList){
                        for(Integer int1:integerList){
                            for(Integer int2:integerList){
                                for(Integer int3:integerList){
                                    for(Integer int4:integerList){
                                        for(Integer int5:integerList){
                                            Card card = new Card(msg, msg1, msg2, int1, int2, int3, int4, int5, msg3);
                                            assertTrue(card.getName() == msg);
                                            assertTrue(card.getDescription() == msg1);
                                            assertTrue(card.getFamily() == msg2);
                                            assertTrue(card.getHp() == int1);
                                            assertTrue(card.getEnergy() == int2);
                                            assertTrue(card.getAttack() == int3);
                                            assertTrue(card.getDefense() == int4);
                                            assertTrue(card.getPrice() == int5);
                                            assertTrue(card.getImgUrl() == msg3);

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
