package com.cardshop.service;

import com.cardshop.model.User;
import org.springframework.http.ResponseEntity;

public interface IExternalService {

    ResponseEntity requestCreateOrUpdateToken(User user);
    ResponseEntity requestCreateToken(User user);
    ResponseEntity requestFindTokenWithTokenHeader(String tokenPublic);
}
