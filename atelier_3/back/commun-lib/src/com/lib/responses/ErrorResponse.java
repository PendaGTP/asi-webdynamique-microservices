package com.lib.responses;

import java.util.List;

public class ErrorResponse extends AbstractResponse{
    private List<String> errors;

    public ErrorResponse(List<String> message) {
        this.errors = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
