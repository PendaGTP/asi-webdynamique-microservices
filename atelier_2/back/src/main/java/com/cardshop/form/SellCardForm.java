package com.cardshop.form;

public class SellCardForm {
    private long card;

    public long getCard() {
        return card;
    }

    public void setCard(long card) {
        this.card = card;
    }
}