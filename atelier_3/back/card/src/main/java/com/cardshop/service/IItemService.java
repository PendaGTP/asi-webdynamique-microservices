package com.cardshop.service;

import com.cardshop.model.Card;
import com.cardshop.model.Item;
import com.lib.enums.ItemStatus;

import java.util.List;
import java.util.Optional;

public interface IItemService {
    List<Item> findItemsByStatus(ItemStatus itemStatus);
    Item findItemByCard(Card card);
    Item addItem(Card card, Long userId);
    List<Item> getItems();
    Optional<Item> getItem(Long id);
    void updateItem(Item item);
}
