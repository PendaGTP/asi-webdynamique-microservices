package com.cardshop.service;

import com.cardshop.model.Token;
import com.cardshop.model.User;
import com.cardshop.utils.RandomString;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;


public interface TokenManagerInterface {
    public Token refreshToken(Token token);
    public Token createTokenForUser(User user);
    public Token updateOrCreateTokenForUser(User user);
}
