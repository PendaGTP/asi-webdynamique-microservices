package com.lib.responses;


import com.lib.model.TokenPublic;

public class TokenResponse extends AbstractResponse {
    private TokenPublic token;

    public TokenResponse(TokenPublic token) {
        this.token = token;
    }


    public TokenPublic getToken() {
        return token;
    }

    public void setToken(TokenPublic token) {
        this.token = token;
    }
}
