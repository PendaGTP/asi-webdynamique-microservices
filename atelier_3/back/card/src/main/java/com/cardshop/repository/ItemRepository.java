package com.cardshop.repository;

import com.cardshop.model.Card;
import com.cardshop.model.Item;
import com.lib.enums.ItemStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ItemRepository extends CrudRepository<Item, Long> {
    List<Item> findAllByStatus(ItemStatus status);
    Item findItemByCard(Card card);
}
