package com.lib.responses;

import com.lib.model.UserPublic;
import org.apache.catalina.User;

public class SimpleDataResponse extends AbstractResponse {
    private UserPublic userPublic;

    public SimpleDataResponse(UserPublic userPublic){ this.userPublic = userPublic;}

    public UserPublic getData(){return userPublic;}

    public void setData(UserPublic userPublic){ this.userPublic = userPublic;}
}
