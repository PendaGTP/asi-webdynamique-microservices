package com.lib.model;


import org.springframework.lang.Nullable;

import com.lib.enums.ItemStatus;


public class ItemPublic {
    private Long id;
    private CardPublic card;

    private UserPublic seller;

    private UserPublic buyer;

    private ItemStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public UserPublic getSeller() {
        return seller;
    }

    public void setSeller(UserPublic seller) {
        this.seller = seller;
    }

    public UserPublic getBuyer() {
        return buyer;
    }

    public void setBuyer(@Nullable UserPublic buyer) {
        this.buyer = buyer;
    }

    public ItemStatus getStatus() {
        return status;
    }

    public void setStatus(ItemStatus status) {
        this.status = status;
    }

    public CardPublic getCard() {
        return card;
    }

    public void setCard(CardPublic card) {
        this.card = card;
    }
}
