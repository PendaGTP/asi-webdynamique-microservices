package com.cardshop.service;

import com.cardshop.model.Token;
import com.cardshop.model.User;
import com.cardshop.repository.TokenRepository;
import com.cardshop.repository.UserRepository;
import com.cardshop.utils.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TokenManager implements TokenManagerInterface {

    @Autowired
    private TokenRepository tokenRepository;

    public static String generateTokenString() {
        int tokenSize = (int) (Math.random()*(100 - 80)) + 80; // between 80 and 100
        return RandomString.randomString(tokenSize);
    }

    public static LocalDate getExpirationDate() {
        LocalDate currentDate = LocalDate.now();
        LocalDate expirationDate = currentDate.plus(2, ChronoUnit.WEEKS);
        return expirationDate;
    }

    public Token refreshToken(Token token) {
        token.setToken(generateTokenString());
        token.setExpirationDate(getExpirationDate());
        tokenRepository.save(token);
        return token;
    }

    public Token createTokenForUser(User user) {
        Token token = new Token();
        token.setToken(generateTokenString());
        token.setExpirationDate(getExpirationDate());
        token.setUser(user);
        tokenRepository.save(token);
        return token;
    }

    public Token updateOrCreateTokenForUser(User user) {
        Token token;
        if(user.getToken() != null) {
            token = refreshToken(user.getToken());
        } else {
            token = createTokenForUser(user);
        }
        return token;
    }

    public User findUserFromTokenString(String tokenString) {
        Token token = tokenRepository.findTokenByToken(tokenString);
        if (token != null) {
            return token.getUser();
        }
        return null;
    }
}

