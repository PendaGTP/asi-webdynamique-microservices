package com.cardshop.service;

import com.cardshop.model.Card;
import com.cardshop.repository.CardRepository;
import com.lib.form.StoreCardForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CardService implements ICardService{

    @Autowired
    private CardRepository cardRepository;


    @Override
    public Card addCard(StoreCardForm storeCardForm, Long userId) {
        Card card = new Card(
                storeCardForm.getName(),
                storeCardForm.getDescription(),
                storeCardForm.getFamily(),
                storeCardForm.getHp(),
                storeCardForm.getEnergy(),
                storeCardForm.getAttack(),
                storeCardForm.getDefense(),
                storeCardForm.getPrice(),
                storeCardForm.getImgUrl(),
                storeCardForm.getUserId()
        );
        card.setUserId(userId);

        return cardRepository.save(card);
    }

    @Override
    public List<Card> getCards(Long userId) {
        return cardRepository.findCardsByUserId(userId);
    }

    @Override
    public Optional<Card> getCard(Long cardId) {
        return cardRepository.findById(cardId);
    }

    @Override
    public void updateCard(Card card) {
        cardRepository.save(card);
    }


}
