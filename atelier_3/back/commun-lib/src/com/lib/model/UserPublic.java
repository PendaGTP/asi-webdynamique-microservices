package com.lib.model;




import java.util.List;

public class UserPublic {
    private Long id;

    private String name;
    private String email;
    private String password;
    private Integer balance;


    private List<CardPublic> cards;

    private TokenPublic token;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return email;
    }

    public void setMail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public TokenPublic getToken() {
        return token;
    }

    public void setToken(TokenPublic token) {
        this.token = token;
    }

    public List<CardPublic> getCards() {
        return cards;
    }

    public void setCards(List<CardPublic> cards) {
        this.cards = cards;
    }
}
