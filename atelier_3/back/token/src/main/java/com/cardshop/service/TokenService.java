package com.cardshop.service;

import com.cardshop.model.Token;
import com.cardshop.repository.TokenRepository;
import com.lib.model.UserPublic;
import com.lib.utils.RandomString;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
public class TokenService implements ITokenService{

    @Autowired
    private TokenRepository tokenRepository;

    public static String generateTokenString() {
        int tokenSize = (int) (Math.random()*(100 - 80)) + 80; // between 80 and 100
        return RandomString.randomString(tokenSize);
    }

    public static LocalDate getExpirationDate() {
        LocalDate currentDate = LocalDate.now();
        LocalDate expirationDate = currentDate.plus(2, ChronoUnit.WEEKS);
        return expirationDate;
    }

    public Long findUserFromTokenString(String tokenString){
        Token token = tokenRepository.findTokenByToken(tokenString);
        if (token != null){
            return token.getUserId();
        }
        return null;
    }


    @Override
    public Token refreshToken(Token token) {
        token.setToken(generateTokenString());
        token.setExpirationDate(getExpirationDate());
        tokenRepository.save(token);
        return token;
    }

    @Override
    public Token addToken(Long userId) {
        Token token = new Token();
        token.setToken(generateTokenString());
        token.setExpirationDate(getExpirationDate());
        token.setUserId(userId);
        return tokenRepository.save(token);
    }

    @Override
    public Token updateOrCreateTokenForUser(Long userTokenId) {
        Token token1;
        Optional<Token> token = tokenRepository.findTokenByUserId(userTokenId);
        if(token.isPresent()){
            token1 = refreshToken(token.get());
        } else {
            token1 = addToken(userTokenId);
        }
        return token1;
    }

    @Override
    public Token findTokenByToken(String token) {
        return tokenRepository.findTokenByToken(token);
    }
}
