package com.cardshop.repository;

import com.cardshop.model.Token;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
public interface TokenRepository extends CrudRepository<Token, Long> {
    Token findTokenByToken(String tokenString);
    Optional<Token> findTokenByUserId(Long userTokenId);
}
