verifyToken();
$(document ).ready(function(){

    $("#playButtonId").click(function(){
        document.location = "../play.html";
    });
    $("#buyButtonId").click(function(){
        document.location = "../buyCard.html";
        //TO DO
    });
    $("#sellButtonId").click(function(){
        document.location = "../sellCard.html";
    });
});



function verifyToken(){
    var token = Cookies.get("token");
    $.ajax({
        url: "http://localhost:8888/auth/me",
        type: "GET",
        beforeSend: function (xhr){
            xhr.setRequestHeader('Authorization', token);
        },
        success: function (data, textStatus, request) {
            if(data.status < 200 || data.status >= 400){
                document.location = "./loginPage.html"
                $("#userNameId").append("LOG IN");
            }
            setUser(data);
        },
        error: function(e) {
            console.log(e)
            document.location = "./loginPage.html"
        }
    });
}

function setUser(data){
    console.log("SET USER");
    $("#userNameId").append(`${data.name}`);
    $("#cash").append(`${data.balance}`);
}