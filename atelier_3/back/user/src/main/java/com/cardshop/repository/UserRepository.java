package com.cardshop.repository;



import com.cardshop.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
public interface UserRepository  extends CrudRepository<User, Long> {
    List<User> findUserByName(String name);
    List<User> findUserByEmail(String email);
    Optional<User> findUserByNameAndPassword(String name, String password);
}
