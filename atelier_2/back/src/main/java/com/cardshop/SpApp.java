package com.cardshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.cardshop.repository")
public class SpApp {

    public static void main(String[] args) {
        SpringApplication.run(SpApp.class,args);
    }
}

