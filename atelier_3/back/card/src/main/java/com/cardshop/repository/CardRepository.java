package com.cardshop.repository;


import com.cardshop.model.Card;
import com.lib.model.UserPublic;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CardRepository extends CrudRepository<Card, Long> {
    List<Card> findCardsByName(String name);
    List<Card> findCardsByUserId(Long userId);
    List<Card> findCardsByUserIdAndNameContainingIgnoreCase(Long userId, String name);
}
