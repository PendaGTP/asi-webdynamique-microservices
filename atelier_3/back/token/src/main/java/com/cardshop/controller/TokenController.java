package com.cardshop.controller;


import com.cardshop.model.Token;
import com.cardshop.service.ITokenService;
import com.lib.form.StoreCardForm;
import com.lib.model.TokenPublic;
import com.lib.model.UserPublic;
import com.lib.responses.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class TokenController {

    @Autowired
    private ITokenService iTokenService;


    // -- Create new token for new user
    @RequestMapping(method = RequestMethod.POST, value = "/token")
    Token addToken(@RequestBody UserPublic userPublic){
        return iTokenService.addToken(userPublic.getId());
    }

    // -- Create or update token for an user
    @RequestMapping(method = RequestMethod.GET, value = "/token", params = "user")
    Token updateOrCreateToken(@RequestParam("user") Long userId){
        System.out.println("TokenController : updateOrCreateToken for userId : "+userId);
        return iTokenService.updateOrCreateTokenForUser(userId);
    }

    // FIND TOKEN BY TOKEN HEADER
    @RequestMapping(method = RequestMethod.GET, value = "/token")
    Token findTokenByTokenHeader(@RequestHeader("Authorization") String tokenHeader) {
        System.out.println("TokenController : findTokenByTokenHeader for tokenHeader : "+tokenHeader);
        return iTokenService.findTokenByToken(tokenHeader);
    }




    /*
    // CREATE TOKEN FOR USER
    @RequestMapping(method = RequestMethod.GET, value = "/token/{userId}")
    public Token getToken(@PathVariable Long userId){
        System.out.println("TokenController : getToken");
        return iTokenService.createTokenForUser(userId);
    }

     */
/*
    // CREATE OR UPDATE A TOKEN FOR A USER
    @RequestMapping(method = RequestMethod.GET, value = "/token/refresh/{id}")
    public Token updateOrCreateToken(@PathVariable Long id){
        System.out.println("TokenController : updateOrCreateToken");
        return iTokenService.updateOrCreateTokenForUser(id);
    }
*/
}
