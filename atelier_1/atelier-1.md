# Atelier 1

## Le desgin pattern MVC

* Qu'est ce que le pattern MVC ?

Tout d'abord, MVC signifie Modèle-Vue-Contrôleur
Le but de MVC est de spérarer la logique du code en tois parties que l'on retrouve dans des fichiers distincts.

Explication des trois parties : 
    
    * Modèle : c'est les données, BDD

    * Vue : c'est l'affichage, elle se contente de récupérer des variables à afficher

    * Contrôleur : c'est la partie logique du code qui prend des décisions


* Quels avantages présente-t-il ?

    * Conception claire : séparation des données, vue et contrôleur
    * Gain de temps -> maintenance facilité
    * Souple : on peut remplacer une partie plus facilement


* Qu’est-ce que le Web dynamique? pourquoi est-il intéressant? 

    * Permet de générer le contenu d'une page web dynamiquement


* Comment sont récupérés les fichiers par le Web Browser en Web statique? 

    *  C'est le serveur web qui renvoit les fichiers au web browser


* Quels sont les avantages d’utiliser du Web Statique avec des services REST

    * Permet de changer de la forme du rendue mais le contenue ne change pas.
    * On peut générer la page au fur et à mesure.
    * On demande seulement les données que l'on à besoin.

* Qu’est-ce que JEE ?  
    * L'appelation JEE n'existe pas, en revanche Java EE oui. 
    * Java EE signifie "Java Enterprise Edition", c'est une plate-forme construite sur le language Java, principalement destinée au développement d'applications web.
    * Cette plate-forme est une extension de Java SE (Standard Edition).

* Comment fonctionne un serveur JEE ?  
    * JVM avec:
        * serveur HTTP pour réception/envois des requêtes/réponses.
        * web container avec JSP (Java Server Page) + servlet pour l'instanciation des classes nécessaires.
    * Création dynamique de page web côté serveur (un peu comme PHP).  
     
* Qu’est-ce qu’un Web Container en JEE ?  
    * Interface entre les composants web (ex: servlet) et le serveur web. 
    * Il distribue les requêtes aux composants de l'application.
    * Fournit aussi des données contextuelles (ex: infos sur la requête actuelle).

## les expressions EL
   
* Qu’est-ce que les expressions EL ?

    * EL = Expression Language
    * utilisation d'expressions de la forme ${bean.attribute} ou ${bean[0]} pour accéder aux données d'un bean

* Que permettent de faire les expressions EL ?

    * accès avec . ou [] aux propriétés
    * possibilité d'utiliser plusieurs termes, séparés chacun par un opérateur
    * types primaire pour les termes :
        * null
        * Integer
        * Double
        * String
        * Boolean
    * types d'opérateurs possibles :
        * Arithmétiques
        * Relationnels
        * Logique
        
    
* Qu’apporte JSTL aux JSP ?

    * permet d'accéder aux données plus simplement
    * s'affranchir de l'accès aux données
    * simplifier le code de la JSP
    
## JDBC et Statement et PrepareStatement

JDBC pour Java DataBase Connectivity.

JDBC permet de se connecter à une base de données (avec un pilote qu'il faut télécharger avant).

Dans Java toute les classes JDBC sont disponible dans le package java.sql (classique il faudra l'importer -> enfin l'IDE va le faire pour nous).

### Installer le driver pour utiliser JDBC 

Pour installer le driver JDBC sur Windows (partie 54.3) : https://www.jmdoudoux.fr/java/dej/chap-jdbc.htm 

### Connexion à une base de données

Ensuite pour se connecter à une base de données, il faut utiliser des méthodes --> classique, voir : https://openclassrooms.com/fr/courses/26832-apprenez-a-programmer-en-java/26258-jdbc-decouvrez-la-porte-dacces-aux-bases-de-donnees#r-26257

### Les requêtes : Statement
Voir : https://openclassrooms.com/fr/courses/26832-apprenez-a-programmer-en-java/26543-fouillez-dans-la-base-de-donnees

Pour éxecuter une requête, il faut un objet **Statement**, celui-ci permet d'exécuter des instructions SQL, il interroge la base de données et retourne les résultats.
L'objet **Statement** est fourni par l'objet **Connection** grâce à l'instruction ```conn.createStatement```.


### Les requeêtes : PrepareStatement

Idem que les objet **Statement** mais permet d'utiliser des paramètres dans nos requêtes et donc d'y insérer des caractères spéciaux, etc.
C'est le même principe qu'en PHP avec l'objet PDO et les requêtes préparées.
## Comment fontionne l'AJAX ?

L'AJAX est donc un ensemble de technologies visant à effectuer des transferts de données.
Le plus souvent utiliser de maniere asynchrone.

## Que représente les Servlet dans JEE ? Qu’est-ce que JSP ? Quel est le cycle de vie d’un JSP ?

### Servelets

Les Servets presentent des requetes HTTP, de la meme maniere que les Web Sockets.
Une servlet est simple classe Java, qui a la particularité de permettre le traitement de requêtes et la personnalisation de réponses.
 
### JSP 

Tout comme le HTML, le JSP est un langage balisé.
Elles permettent tout type d'actions comme le fait d'ecrire un algorithme dans du code html ou le fait d'inclure d'autres fichiers.

### Cycle de vie d'une JSP

Un cycle de vie d’une JSP peut être définie comme l’ensemble du processus depuis sa création jusqu’à la destruction qui est similaire à un cycle de vie d’une servlet avec une étape supplémentaire qui est nécessaire pour compiler une JSP en servlet.
Les differentes etapes sont :
- Compilation
- Initialisation
- Exécution
- Nettoyage


## JavaBean

* Qu’est-ce qu’un Javabean ? 

    *  c'est un composant réutilisable
    *

* Quels sont ces propriétés/contraintes ? 

* Comment utilise-t-on les Javabean avec les JSP ?
