package com.cardshop.service;


import com.cardshop.model.User;
import com.cardshop.repository.UserRepository;
import com.lib.form.RegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService implements IUserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public User addUser(RegisterForm registerForm) {
        User user = new User();
        user.setName(registerForm.getName());
        user.setMail(registerForm.getEmail());
        user.setPassword(registerForm.getPassword());
        user.setBalance(5000);
        return userRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public Boolean userAlreadyExist(String username){
        if (getUsersByName(username).size() != 0){
            return true;
        } else {return false;}
    }

    @Override
    public Boolean passwordComplexityValidation(String password){
        if (password.length() <= 8) {
            return false;
        } else {
          return true;
        }
    }

    @Override
    public User login(String name, String password) {
        // TODO : hash password :)
        Optional<User> user = userRepository.findUserByNameAndPassword(name, password);
        if (user.isPresent()) {
            return user.get();
        }else {
            return null;
        }
    }

    @Override
    public List<User> getUsersByName(String name) {
        return userRepository.findUserByName(name);
    }

    @Override
    public Optional<User> getuser(Long userId) {
        return userRepository.findById(userId);
    }

}
