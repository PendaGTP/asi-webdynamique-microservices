package com.cardshop.controller;

import com.cardshop.form.RegisterForm;
import com.cardshop.form.StoreCardForm;
import com.cardshop.model.Card;
import com.cardshop.model.User;
import com.cardshop.repository.CardRepository;
import com.cardshop.responses.DataResponse;
import com.cardshop.responses.HttpResponse;
import com.cardshop.service.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cards")
public class CardController {
    @Autowired
    private CardRepository cardRepository;
    @Autowired
    private TokenManager tokenManager;

    @GetMapping(value = "/", produces = "application/json")
    @ResponseBody
    ResponseEntity index(@RequestHeader("Authorization") String tokenHeader) {
        User user = tokenManager.findUserFromTokenString(tokenHeader);
        if(user != null) {
            List<Card> cards = cardRepository.findCardsByUser(user);
            return ResponseEntity.ok(new DataResponse(cards));
        }
        return ResponseEntity.status(401).body(new HttpResponse(
                401, "Access not allowed"
        ));
    }

    @PostMapping(value = "/", produces = "application/json")
    @ResponseBody
    ResponseEntity store(@RequestHeader("Authorization") String tokenHeader, @RequestBody StoreCardForm storeCardForm) {
        User user = tokenManager.findUserFromTokenString(tokenHeader);
        if(user != null) {
            Card card = new Card(
                storeCardForm.getName(),
                storeCardForm.getDescription(),
                storeCardForm.getFamily(),
                storeCardForm.getHp(),
                storeCardForm.getEnergy(),
                storeCardForm.getAttack(),
                storeCardForm.getDefense(),
                storeCardForm.getPrice(),
                storeCardForm.getImgUrl()
            );

            card.setUser(user);
            cardRepository.save(card);
            List<Card> cards = cardRepository.findCardsByUser(user);
            return ResponseEntity.ok(new DataResponse(cards));
        }
        return ResponseEntity.status(401).body(new HttpResponse(
                401, "Access not allowed"
        ));
    }

    @GetMapping(value = "/", produces = "application/json", params = "name")
    @ResponseBody
    ResponseEntity store(@RequestHeader("Authorization") String tokenHeader, @RequestParam("name") String name) {
        User user = tokenManager.findUserFromTokenString(tokenHeader);
        if(user != null) {
            List<Card> cards = cardRepository.findCardsByUserAndNameContainingIgnoreCase(user, name);
            return ResponseEntity.ok(new DataResponse(cards));
        }
        return ResponseEntity.status(401).body(new HttpResponse(
                401, "Access not allowed"
        ));
    }


}
