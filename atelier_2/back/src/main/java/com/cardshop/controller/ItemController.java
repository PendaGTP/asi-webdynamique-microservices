package com.cardshop.controller;

import com.cardshop.enums.ItemStatus;
import com.cardshop.form.SellCardForm;
import com.cardshop.model.Card;
import com.cardshop.model.Item;
import com.cardshop.model.User;
import com.cardshop.repository.CardRepository;
import com.cardshop.repository.ItemRepository;
import com.cardshop.repository.UserRepository;
import com.cardshop.responses.DataResponse;
import com.cardshop.responses.HttpResponse;
import com.cardshop.service.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/v1/items")
public class ItemController {

    // TODO:  CREATE A ShopManager
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CardRepository cardRepository;
    @Autowired
    private TokenManager tokenManager;

    @GetMapping(value = "/", produces = "application/json")
    @ResponseBody
    ResponseEntity index(@RequestHeader("Authorization") String tokenHeader) {
        User user = tokenManager.findUserFromTokenString(tokenHeader);
        if(user != null) {
            List<Item> items = itemRepository.findAllByStatus(ItemStatus.PENDING);
            return ResponseEntity.ok(new DataResponse(items));
        }
        return ResponseEntity.status(401).body(new HttpResponse(
                401, "Access not allowed"
        ));
    }

    @PostMapping(value = "/", produces = "application/json")
    @ResponseBody
    ResponseEntity sell(@RequestHeader("Authorization") String tokenHeader, @RequestBody SellCardForm sellCardForm) {
        User user = tokenManager.findUserFromTokenString(tokenHeader);
        if(user != null) {
            Optional<Card> card = cardRepository.findById(sellCardForm.getCard());
            if(card.isPresent()){
                if(itemRepository.findItemByCard(card.get()) != null) {
                    return ResponseEntity.status(400).body(new HttpResponse(
                            400, "La carte est deja en vente !"
                    ));
                }
                Item item = new Item();
                item.setCard(card.get());
                item.setSeller(user);
                item.setStatus(ItemStatus.PENDING);
                itemRepository.save(item);
                List<Item> items = (List<Item>) itemRepository.findAll();
                return ResponseEntity.ok(new DataResponse(items));
            }
            return ResponseEntity.status(420).body(new HttpResponse(
                    420, "Cette carte n'existe pas"
            ));
        }
        return ResponseEntity.status(401).body(new HttpResponse(
                401, "Access not allowed"
        ));
    }

    @PatchMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    ResponseEntity buy(@RequestHeader("Authorization") String tokenHeader, @PathVariable Long id) {
        User user = tokenManager.findUserFromTokenString(tokenHeader);
        if(user != null) {
            Optional<Item> optionalItem = itemRepository.findById(id);
            if(optionalItem.isPresent() && optionalItem.get().getStatus() == ItemStatus.PENDING){
                Item item = optionalItem.get();
                Card card = item.getCard();
                if(user.getBalance() >= card.getPrice()) {
                    user.setBalance(user.getBalance() - item.getCard().getPrice());
                    userRepository.save(user);
                    card.setUser(user);
                    cardRepository.save(card);
                    item.setBuyer(user);
                    item.setStatus(ItemStatus.SOLD);
                    itemRepository.save(item);
                    return ResponseEntity.ok(item.getCard());
                }

            }
            return ResponseEntity.status(420).body(new HttpResponse(
                    409, "Cette item n'existe pas ou n\'est plus en vente"
            ));
        }
        return ResponseEntity.status(401).body(new HttpResponse(
                401, "Access not allowed"
        ));
    }

}
