package com.cardshop.repository;

import com.cardshop.model.Card;
import com.cardshop.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CardRepository extends CrudRepository<Card, Long> {
    List<Card> findCardsByName(String name);
    List<Card> findCardsByUser(User user);
    List<Card> findCardsByUserAndNameContainingIgnoreCase(User user, String name);
}
