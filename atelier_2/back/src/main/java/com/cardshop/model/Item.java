package com.cardshop.model;

// imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price

import com.cardshop.enums.ItemStatus;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "items")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne()
    private Card card;

    @ManyToOne()
    private User seller;

    @ManyToOne()
    @Nullable
    private User buyer;

    @Enumerated(EnumType.ORDINAL)
    private ItemStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    @Nullable
    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(@Nullable User buyer) {
        this.buyer = buyer;
    }

    public ItemStatus getStatus() {
        return status;
    }

    public void setStatus(ItemStatus status) {
        this.status = status;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
