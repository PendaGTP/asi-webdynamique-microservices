package com.cardshop.service;

import com.cardshop.model.Token;
import com.lib.model.UserPublic;

public interface ITokenService {
    Token refreshToken(Token token);
    Token addToken(Long userId);
    Token updateOrCreateTokenForUser(Long userTokenId);
    Token findTokenByToken(String token);
}
