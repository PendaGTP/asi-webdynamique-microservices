package com.cardshop.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class CardTest {

    private String name = "name";
    private String description = "description";
    private String family = "family";
    private Integer hp = 1000;
    private Integer energy = 800 ;
    private Integer attack = 333;
    private Integer defense = 200;
    private Integer price = 100;
    private String imgUrl = "imgUrl";
    private Long userId = 1L;
    private Long tokenId = 1L;

    private Card card;

    @Before
    public void setUp() {
        System.out.println("[BEFORE TEST] : -- Add Card to test");
        card = new Card(name,description,family,hp,energy,attack,defense,price,imgUrl,userId);
    }

    @After
    public void cleanUp(){
        System.out.print("[AFTER TEST] - Clean hero list");
        card = null;
    }


    @Test
    public void createCard() {
        Card card = new Card(name, description, family, hp, energy, attack, defense, price, imgUrl, userId);
        assertTrue(card.getName() == name);
        assertTrue(card.getDescription() == description);
        assertTrue(card.getFamily() == family);
        assertTrue(card.getHp() == hp);
        assertTrue(card.getEnergy() == energy);
        assertTrue(card.getAttack() == attack);
        assertTrue(card.getDefense() == defense);
        assertTrue(card.getPrice() == price);
        assertTrue(card.getImgUrl() == imgUrl);
        assertTrue(card.getUserId() == userId);
    }
}
