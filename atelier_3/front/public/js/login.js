// attendre le chargement de la page
$(document ).ready(function() {
    console.log("coucou");
    // EVENT : click sur le bouton submit
    $("#submit").on("click", () => {
        console.log("coucou");
        requestLogin($("#username").val(), $("#password").val());
    });


});

function requestLogin( name, password ){
    $.ajax({
        url: "http://localhost:8888/auth/login/",
        type: "POST",
        data: JSON.stringify({
            name,
            password
        }),
        contentType: "application/json",
        dataType: "json",
        success: function (data, textStatus, request) {
            console.log(data);
            let headers = request.getResponseHeader('Authorization');
            console.log(headers);
            Cookies.set("token", data['token']['token']);
            document.location = "./cardHome.html"; //http://localhost:8080
        },
        error: function(e) {
            console.log(e)
            alert("Identifiant ou mot de passe incorret.");
        }
    });
}
