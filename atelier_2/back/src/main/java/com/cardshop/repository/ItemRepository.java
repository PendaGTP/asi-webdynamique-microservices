package com.cardshop.repository;

import com.cardshop.enums.ItemStatus;
import com.cardshop.model.Card;
import com.cardshop.model.Item;
import com.cardshop.model.Token;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface ItemRepository extends CrudRepository<Item, Long> {
    public List<Item> findAllByStatus(ItemStatus status);
    public Item findItemByCard(Card card);
}

