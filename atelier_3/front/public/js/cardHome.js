verifyToken();
$(document ).ready(function(){

    $("#playButtonId").click(function(){
        document.location = "../play.html";
    });
    $("#buyButtonId").click(function(){
        document.location = "../buyCard.html";
        //TO DO
    });
    $("#sellButtonId").click(function(){
        document.location = "../sellCard.html";
    });
});



function verifyToken(){
    console.log("Verifing token");
    var token = Cookies.get("token");
    $.ajax({
        url: "http://localhost:8888/auth/me",
        type: "GET",
        beforeSend: function (xhr){
            xhr.setRequestHeader('Authorization', token);
        },
        success: function (data, textStatus, request) {
            if(data.status < 200 || data.status >= 400){
                document.location = "./loginPage.html"
                $("#userNameId").append("LOG IN");
            }
            console.log(data);
            setUser(data);
        },
        error: function(e) {
            console.log(e)
            document.location = "./loginPage.html"
        }
    });
}

function setUser(data){
    console.log("SET USER : "+data.name);
    $("#userNameId").append(`${data.userId}`);
    $("#cash").append(`${data.balance}`);
}