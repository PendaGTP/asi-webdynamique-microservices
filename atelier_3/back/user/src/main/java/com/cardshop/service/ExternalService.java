package com.cardshop.service;

import com.cardshop.model.User;
import com.lib.model.TokenPublic;
import com.lib.responses.ErrorResponse;
import com.lib.responses.HttpResponse;
import com.lib.responses.TokenResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExternalService implements IExternalService{
    @Override
    public ResponseEntity requestCreateOrUpdateToken(User user) {
        System.out.println("ExternalService : requestCreateOrUpdateToken for userId :"+ user.getId());

        // Array pour les erreurs
        List<String> errors = new ArrayList<>();

        // Token-Service config
        RestTemplate restTemplate = new RestTemplate();
        String tokenResourceUrl = "http://token:8484/token?user="+user.getId();

        // Envoie de la requête
        ResponseEntity<TokenPublic> responseEntity = restTemplate.getForEntity(tokenResourceUrl, TokenPublic.class);

        // Gestion de la réponse
        if(responseEntity.getStatusCodeValue() != 200){
            errors.add("an error occur in server side error n°: "+responseEntity.getStatusCodeValue());
            return ResponseEntity.status(500).body(new ErrorResponse(errors));
        }else {
            return ResponseEntity.ok(new TokenResponse(responseEntity.getBody()));
        }
    }

    @Override
    public ResponseEntity requestCreateToken(User user) {
        System.out.println("ExternalService : requestCreateToken for userId :"+ user.getId());

        // Array pour les erreurs
        List<String> errors = new ArrayList<>();

        // Token-Service config
        RestTemplate restTemplate = new RestTemplate();
        String tokenResourceUrl = "http://token:8484/token";

        // Envoie de la requête
        ResponseEntity<TokenPublic> responseEntity = restTemplate.postForEntity(tokenResourceUrl, user, TokenPublic.class);

        // Gestion de la réponse
        if(responseEntity.getStatusCodeValue() != 200){
            errors.add("an error occur in server side error n°: "+responseEntity.getStatusCodeValue());
            return ResponseEntity.status(500).body(new ErrorResponse(errors));
        }else {
            return ResponseEntity.ok(new TokenResponse(responseEntity.getBody()));
        }
    }

    @Override
    public ResponseEntity requestFindTokenWithTokenHeader(String tokenHeader) {
        System.out.println("ExternalService : requestFindTokenWithTokenHeader for tokenHeader :"+tokenHeader);

        // Array pour les erreurs
        List<String> errors = new ArrayList<>();

        // -- Token-Service config
        // header setup
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", tokenHeader);
        HttpEntity headersHttp = new HttpEntity(headers);

        RestTemplate restTemplate = new RestTemplate();
        String tokenResourceUrl = "http://token:8484/token";

        // Envoie de la requête
        ResponseEntity<TokenPublic> response = restTemplate.exchange(
                tokenResourceUrl, HttpMethod.GET, headersHttp, TokenPublic.class);

        // Gestion de la réponse
        if (response.getStatusCodeValue() == 200){
            System.out.println("token trouvé tiens : "+response.getBody());
            return ResponseEntity.ok(new TokenResponse(response.getBody()));
        }else if(response.getStatusCodeValue() == 401){
            return ResponseEntity.status(401).body(new HttpResponse(401, "Access non autorisé"));
        }else {
            errors.add("an error occur in server side");
            return ResponseEntity.status(response.getStatusCodeValue()).body(new ErrorResponse(errors));
        }
    }
}
