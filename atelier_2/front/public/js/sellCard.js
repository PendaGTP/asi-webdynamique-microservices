var id = 0;
$(document ).ready(function() {

    const card = new Vue({el: "#vueRoot"});
    // enable Vue.js on #cardList elementconst card = new Vue({el: "#vueRoot", data: {keyComponent : 0}});

    $.ajax({
        url: "http://localhost:8888/auth/me",
        type: "GET",
        beforeSend: function (xhr){
            xhr.setRequestHeader('Authorization', Cookies.get("token"));
        },
        success: function (data, textStatus, request) {
            console.log(data);
            let headers = request.getResponseHeader('Authorization');
            console.log(request);
            setUser(data);
        },
        error: function(e) {
            console.log(e)
        }
    });

    $.ajax({
        url: "http://localhost:8888/api/v1/cards/",
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        beforeSend: function (xhr){
            xhr.setRequestHeader('Authorization', Cookies.get("token"));
        },
        success: function (data, textStatus, request) {
            console.log(data);
            let headers = request.getResponseHeader('Authorization');
            console.log(request);
            //Cookies.set("token", data['token']['token']);
            generateTable(data, card);
        },
        error: function(e) {
            console.log(e)
        }
    });


    $("#vendre").on('click', () => {
        $.ajax({
            url: "http://localhost:8888/api/v1/items/",
            type: "POST",
            data: JSON.stringify({
                card: id,
            }),
            contentType: "application/json",
            dataType: "json",
            beforeSend: function (xhr){
                xhr.setRequestHeader('Authorization', Cookies.get("token"));
            },
            success: function (data, textStatus, request) {
                console.log(data);
                location.reload();
            },
            error: function(e) {
                console.log(e)
            }
        });
    });

    $('#searchButtonId').on('click', () => {
        let search = $("#searchId").val();
        let requestedUrl = "http://localhost:8888/api/v1/cards/?name=" + $("#searchId").val();
        $.ajax({
            url: requestedUrl,
            type: "GET",
            contentType: "application/json",
            dataType: "json",
            beforeSend: function (xhr){

                xhr.setRequestHeader('Authorization', Cookies.get("token"));
            },
            success: function (data, textStatus, request) {
                console.log(data);
                if(request.status >= 200 || request.status < 400){
                    generateTable(data, card);
                }

            },
            error: function(e) {
                console.log(e)
            }
        });
    });
});


function generateTable(data, card){
    $("#table").html("");
    for(let i = data.data.length - 1; i >= 0 ; i--){
        let dataRow = data.data[i];
        let row = document.createElement("tr");
        $(row).on('click', () => {
            changeCardData(dataRow);
            id = dataRow.id;
            console.log(id);
            card = new Vue({el: "#vueRoot"});
        })
        $(row).append(`<td>${dataRow.name}</td><td>${dataRow.price}</td><td>${dataRow.attack}</td><td>${dataRow.defense}</td><td>${dataRow.energy}</td><td>${dataRow.hp}</td>`)
        $("#table").append(row);

    }

}

function changeCardData(cardData){
    $('#vueRoot').html(`<card verbe="vendre" src-img-family="" price="${cardData.price}" family-name="${cardData.family}" src-image-Hero="${cardData.imgUrl}" name-Hero="${cardData.name}" description-Hero="${cardData.description}" Hp-Hero="${cardData.hp}" Nrj-Hero="${cardData.energy}" Atk-Hero="${cardData.attack}" Def-Hero="${cardData.defense}"></card>`)

}

function setUser(data){
    console.log("SET USER");
    $("#userNameId").append(`${data.name}`);
    $("#cash").append(`${data.balance}`);
}