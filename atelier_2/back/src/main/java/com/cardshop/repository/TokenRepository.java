package com.cardshop.repository;

import com.cardshop.model.Token;
import com.cardshop.model.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface TokenRepository extends CrudRepository<Token, Long> {
    public Token findTokenByToken(String token);
}

