package com.lib.model;

public class GamePublic {

    private Long id;


    private CardPublic card1;

    private CardPublic card2;


    private UserPublic player1;

    private UserPublic player2;

    private UserPublic winner;

    private Integer bet;

    public Integer getBet() {
        return bet;
    }

    public void setBet(Integer bet) {
        this.bet = bet;
    }
}
