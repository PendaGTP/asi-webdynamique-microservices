package com.lib.form;


public class StoreCardForm {
    private String name;
    private String description;
    private String family;
    private Integer hp;
    private Integer energy;
    private Integer attack;
    private Integer defense;
    private Integer price;
    private String imgUrl;
    private Long userId;
    private Long tokenId;

    public StoreCardForm(){

    }

    public StoreCardForm(String name, String description, String family,
                         Integer hp, Integer energy, Integer attack, Integer defense,
                         Integer price, String imgUrl){
        this.name = name;
        this.description = description;
        this.family = family;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.defense = defense;
        this.price = price;
        this.imgUrl = imgUrl;
    }

    public StoreCardForm(String name, String description, String family,
                         Integer hp, Integer energy, Integer attack, Integer defense,
                         Integer price, String imgUrl, Long userId, Long tokenId){
        super();
        this.name = name;
        this.description = description;
        this.family = family;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.defense = defense;
        this.price = price;
        this.imgUrl = imgUrl;
        this.userId = userId;
        this.tokenId = tokenId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getEnergy() {
        return energy;
    }

    public void setEnergy(Integer energy) {
        this.energy = energy;
    }

    public Integer getAttack() {
        return attack;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    public Integer getDefense() {
        return defense;
    }

    public void setDefense(Integer defense) {
        this.defense = defense;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTokenId() {
        return tokenId;
    }

    public void setTokenId(Long tokenId) {
        this.tokenId = tokenId;
    }
}
