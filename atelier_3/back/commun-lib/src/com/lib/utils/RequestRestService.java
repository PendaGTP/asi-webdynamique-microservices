package com.lib.utils;
import org.springframework.web.client.RestTemplate;

public class RequestRestService {

    static final String baseUrl = "http://localhost";
    private String completeUrl;
    private RestTemplate restTemplate;

    /*
    8181 : room
    8282 : card
    8383: utilisteur
    8484 : token
    */

    public RequestRestService(String endpoint){
        switch(endpoint){
            case "/room":
                completeUrl = baseUrl + ":8181" + endpoint;
                break;
            case "/card":
                completeUrl = baseUrl + ":8282" + endpoint;
                break;
            case "/user":
                completeUrl = baseUrl + ":8383" + endpoint;
                break;
            case "/token":
                completeUrl = baseUrl + ":8484" + endpoint;
                break;
        }
        restTemplate = new RestTemplate();
    }
/*
    public String get(String[] args, Class expectedResponseClass) {



            // Send request with GET method and default Headers.

        String result = restTemplate.getForObject(completeUrl, expectedResponseClass.class);

        System.out.println(result);
    }
*/
}

