package com.cardshop.repository;

import com.cardshop.model.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface UserRepository extends CrudRepository<User, Long> {
    public List<User> findUsersByName(String name);
    public List<User> findUsersByEmail(String email);
    public Optional<User> findUserByNameAndPassword(String name, String password);
}
