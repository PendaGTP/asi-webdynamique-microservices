package com.lib.responses;

import org.springframework.ui.Model;

import java.util.List;

public class DataResponse<E> extends AbstractResponse {
    private List<E> data;

    public DataResponse(List<E> data) {
        this.data = data;
    }

    public List<E> getData() {
        return data;
    }

    public void setData(List<E> data) {
        this.data = data;
    }
}
