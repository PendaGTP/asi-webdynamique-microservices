Vue.component('card', {
    props: ['price', 'verbe', 'srcImgFamily', 'familyName', 'srcImageHero', 'nameHero', 'descriptionHero', 'HpHero', 'NrjHero', 'AtkHero', 'DefHero'],

    template: ' <div class="ui five column grid">\n' +
        '            <div class="column"></div>\n' +
        '            <div class="column"></div>\n' +
        '            <div class="column">\n' +
        '                <div class="ui special cards">\n' +
        '                    <div class="card">\n' +
        '\n' +
        '                        <div class="content">\n' +
        '                            <img id="cardFamilyImgId"  class="ui avatar image" > <span id="cardFamilyNameId">{{familyName}}</span>\n' + //:src="srcImgFamily"
        '                        </div>\n' +
        '                        <div class="image imageCard">\n' +
        '                            <div class="blurring dimmable image">\n' +
        '                                <div class="ui inverted dimmer">\n' +
        '                                    <div class="content">\n' +
        '                                        <div class="center">\n' +
        '                                            <div class="ui primary button">Add Friend</div>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                                <img  id="cardImgId" class="ui centered tiny image">\n' + //:src="srcImageHero"
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="content">\n' +
        '                            <div class="ui form tiny">\n' +
        '                                <div class="field">\n' +
        '                                    <label id="cardNameId">{{nameHero}}</label>\n' +
        '                                    <textarea id="cardDescriptionId" class="overflowHiden" readonly="" rows="5">{{descriptionHero}}</textarea>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="content">\n' +
        '                            <i class="heart outline icon"></i><span id="cardHPId">{{HpHero}}</span> \n' +
        '                            <div class="right floated ">\n' +
        '                                <i class="lightning outline icon"></i>\n' +
        '                                <span id="cardEnergyId">{{NrjHero}}</span> \n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="content">\n' +
        '                            <span class="right floated">\n' +
        '                                <i class=" wizard icon"></i>\n' +
        '                                <span id="cardAttackId">{{AtkHero}}</span> \n' +
        '                            </span>\n' +
        '                            <i class="protect icon"></i>\n' +
        '                           <span id="cardDefenceId">{{DefHero}}</span> \n' +
        '                        </div>\n' +
        '    {{verbe}} pour {{price}}\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>',


});