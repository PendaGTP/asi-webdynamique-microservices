# Atelier 2
* Qu’est ce que des Services Web Full Rest ? Quelles sont les contraintes imposées par ce type de
service ?



* Qu’est ce qu’un gestionnaire de dépendance ? Maven est-il le seul ? Quel est l’avantage d’utiliser un
  gestionnaire de dépendances ? Quelles sont les grandes étapes de fonctionnement de Maven ?
  
  Le gestionnaire de dépendance se charge de télécharger toutes les dépendances requise pour un projet. Maven n'est pas le seul, Composer en est un autre (principalement pour PHP).  
  Les avantages d'utiliser un gestionnaire de dépendances:
  * Faciliter le process de build
  * Fournir un système de build uniforme
  * Fournir des informations claires sur le projet
  * Encourager les bonnes pratiques de développement  
    
  Les grandes étapes de fonctionnement de Maven:
  * compilation: compile le code source
  * test: effectue des tests unitaires
  * package: creer des packages (ex: war/jar)
  * installation: stocke le package buildé dans le dossier local Maver
  * deploiement: stocke dans un dossier distant pour partager  
  
* Qu’est ce qu’une application classique Entreprise Java Application ? Donner un exemple d’usage avec
  web service, JSP, JavaBean, EJB et JMS
  

  
* Qu’est ce que Spring ? qu’apporte Spring boot vis-à-vis de Spring ?

Spring est un framework Java. Il fournit notamment des modules comme Sping JDBC.  
Spring boot est une extension de Spring simplifiant la configuration de ce dernier. Il ajoute aussi un serveur embarqué pour simplifier le déploiement etc.

* Qu’est ce que Spring Boot ? Quels sont les points communs/différences entre JEE et Spring Boot ?



* Qu’est ce qu’une annotation ? Quels apports présentent les Annotations ?

* Comment fait-on pour créer un Web Service Rest avec Spring Boot ? 

* Qu’est ce qu’un container de Servlet ? Comment fonctionne un container de Servlet ?

* Expliquer la philosophie « Convention over Configuration » de Spring boot ?

* Expliquer ce qu’il se passe lors de l’exécution «SpringApplication .run(App.class,args) »

* Qu’est ce qu’un DAO ? En quoi est-ce intéressant d’utiliser ce pattern ? Qu’est ce qu’un Singleton ?
  Que permet de réaliser les Entity dans Spring boot ? Est-ce spécifique à SpringBoot ?

* Combien d’instances avez-vous crées lors de l’usage de «Service » en Spring boot? Pourquoi ?

* Que fournit le CRUD Repository de Spring boot ? Que sont les CRUD ?

* Qui réalise l’implémentation de la méthode findByProperty lors de la création d’un repository en
  Spring Boot ?

* Comment gère -t-on les relations One To One, One to Many et Many to Many avec JPA ?

* Qu’est ce qu’une Architecture SOA ? Qu’est ce qu’une architecture Micro Service ? Il y a-t-il d’autres
  architectures existantes ? Quels sont leurs avantages/inconvénients ?
