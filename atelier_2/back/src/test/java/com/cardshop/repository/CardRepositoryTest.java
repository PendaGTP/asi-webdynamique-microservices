package com.cardshop.repository;

import com.cardshop.model.Card;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@DataJpaTest

public class CardRepositoryTest {

    @Autowired
    CardRepository cardRepository;

    @Before
    public void setUp(){
        System.out.println("[BEFORE TEST] : -- Add Card to repository");
        cardRepository.save(new Card("name1","description1","family1",10,20,30,40,50,"http://"));
    }

    @After
    public void cleanUp() {
        cardRepository.deleteAll();
    }

    @Test
    public void saveCard() {
        cardRepository.save(new Card("name","description","family", 10,20,30,40,50,"http://"));
    }

    @Test
    public void saveAndGetCard() {
        cardRepository.deleteAll();

        cardRepository.save(new Card("nameTest","descriptionTest","familyTest", 10,20,30,40,50,"http://"));
        List<Card> cardList = new ArrayList<>();
        cardRepository.findAll().forEach(cardList::add);

        assertTrue(cardList.size() == 1);
        assertTrue(cardList.get(0).getName().equals("nameTest"));
        assertTrue(cardList.get(0).getDescription().equals("descriptionTest"));
        assertTrue(cardList.get(0).getFamily().equals("familyTest"));
        assertTrue(cardList.get(0).getHp().equals(10));
        assertTrue(cardList.get(0).getEnergy().equals(20));
        assertTrue(cardList.get(0).getAttack().equals(30));
        assertTrue(cardList.get(0).getDefense().equals(40));
        assertTrue(cardList.get(0).getPrice().equals(50));
        assertTrue(cardList.get(0).getImgUrl().equals("http://"));
    }


    @Test
    public void getCard() {
        List<Card> cardList = cardRepository.findCardsByName("name1");
        assertTrue(cardList.size() ==1);
        assertTrue(cardList.get(0).getName().equals("name1"));
        assertTrue(cardList.get(0).getDescription().equals("description1"));
        assertTrue(cardList.get(0).getFamily().equals("family1"));
        assertTrue(cardList.get(0).getHp().equals(10));
        assertTrue(cardList.get(0).getEnergy().equals(20));
        assertTrue(cardList.get(0).getAttack().equals(30));
        assertTrue(cardList.get(0).getDefense().equals(40));
        assertTrue(cardList.get(0).getPrice().equals(50));
        assertTrue(cardList.get(0).getImgUrl().equals("http://"));
    }

    @Test
    public void findByName() {
        cardRepository.save(new Card("nameTest1","description","family", 10,20,30,40,50,"http://"));
        cardRepository.save(new Card("nameTest2","description","family", 10,20,30,40,50,"http://"));
        cardRepository.save(new Card("nameTest2","description","family", 10,20,30,40,50,"http://"));
        List<Card> cardList = new ArrayList<>();
        cardRepository.findCardsByName("nameTest2").forEach(cardList::add);
        assertTrue(cardList.size() == 2);
    }


}
